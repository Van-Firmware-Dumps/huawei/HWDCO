
# Copyright (c) 2019-2021, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Changes from Qualcomm Innovation Center are provided under the following
# license:
#
# Copyright (c) 2021-2022 Qualcomm Innovation Center, Inc. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted (subject to the limitations in the
# disclaimer below) provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#
#     * Neither the name of Qualcomm Innovation Center, Inc. nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
# GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
# HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import /vendor/etc/init/hw/init.qti.kernel.rc

on property:persist.sys.temp.overload=1
    write /sys/class/thermal/thermal_zone1/trip_point_1_temp 55000
    write /sys/class/thermal/thermal_zone1/sustainable_power 1000
    write /sys/class/thermal/thermal_zone1/cdev0_weight 1024
    write /sys/class/thermal/thermal_zone1/cdev1_weight 1024
    write /sys/class/thermal/thermal_zone1/cdev2_weight 1024
    write /sys/class/thermal/thermal_zone1/cdev3_weight 1024
    write /sys/class/thermal/thermal_zone1/k_po 50
    write /sys/class/thermal/thermal_zone1/k_pu 100
    write /sys/class/thermal/thermal_zone1/k_i 10
    write /dev/kmsg "boot temp overload, limit board temp."

on property:ro.vendor.user_hota_update=1
    write /sys/class/thermal/thermal_zone1/trip_point_1_temp 75000
    write /sys/class/thermal/thermal_zone1/sustainable_power 9000
    write /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq 691200
    write /sys/devices/system/cpu/cpu4/cpufreq/scaling_max_freq 1113600
    write /sys/devices/system/cpu/cpu7/cpufreq/scaling_max_freq 1286400
    write /dev/kmsg "first boot, user_hota_update = 1.  limit cpufreq."

on early-init
    write /proc/sys/kernel/printk_devkmsg ratelimited
    export MEMTAG_OPTIONS off
    write /proc/boottime "INIT: on init start"
on init
    # Scheduler uclamp
    mkdir /dev/cpuctl/foreground
    mkdir /dev/cpuctl/background
    mkdir /dev/cpuctl/top-app
    mkdir /dev/cpuctl/rt
    mkdir /dev/cpuctl/vip
    mkdir /dev/cpuctl/graphic
    mkdir /dev/cpuctl/system-background
    mkdir /dev/cpuctl/key-background
    mkdir /dev/cpuctl/boost

    chown system system /dev/cpuctl
    chown system system /dev/cpuctl/foreground
    chown system system /dev/cpuctl/background
    chown system system /dev/cpuctl/top-app
    chown system system /dev/cpuctl/rt
    chown system system /dev/cpuctl/vip
    chown system system /dev/cpuctl/graphic
    chown system system /dev/cpuctl/system-background
    chown system system /dev/cpuctl/key-background
    chown system system /dev/cpuctl/boost

    chown system system /dev/cpuctl/tasks
    chown system system /dev/cpuctl/foreground/tasks
    chown system system /dev/cpuctl/background/tasks
    chown system system /dev/cpuctl/top-app/tasks
    chown system system /dev/cpuctl/rt/tasks
    chown system system /dev/cpuctl/vip/tasks
    chown system system /dev/cpuctl/graphic/tasks
    chown system system /dev/cpuctl/system-background/tasks
    chown system system /dev/cpuctl/key-background/tasks
    chown system system /dev/cpuctl/boost/tasks

    chmod 0664 /dev/cpuctl/tasks
    chmod 0664 /dev/cpuctl/foreground/tasks
    chmod 0664 /dev/cpuctl/background/tasks
    chmod 0664 /dev/cpuctl/top-app/tasks
    chmod 0664 /dev/cpuctl/rt/tasks
    chmod 0664 /dev/cpuctl/vip/tasks
    chmod 0664 /dev/cpuctl/graphic/tasks
    chmod 0664 /dev/cpuctl/system-background/tasks
    chmod 0664 /dev/cpuctl/key-background/tasks
    chmod 0664 /dev/cpuctl/boost/tasks

    write /dev/cpuctl/foreground/cpu.rt_runtime_us 950000
    write /dev/cpuctl/background/cpu.rt_runtime_us 950000
    write /dev/cpuctl/top-app/cpu.rt_runtime_us 950000
    write /dec/cpuctl/top-app/cpu.uclamp.rtg_colocate 1
    write /dev/cpuctl/rt/cpu.rt_runtime_us 950000

    # cpld sleep node
    chown system system /sys/bus/platform/devices/cpld_device.0/sleep_node
    chmod 0666 /sys/bus/platform/devices/cpld_device.0/sleep_node

    wait /dev/block/platform/soc/${ro.boot.bootdevice}
    symlink /dev/block/platform/soc/${ro.boot.bootdevice} /dev/block/bootdevice
    chown system system /sys/devices/platform/soc/1d84000.ufshc/auto_hibern8
    chmod 0660 /sys/devices/platform/soc/1d84000.ufshc/auto_hibern8
    start logd

on early-fs
    start vold

on fs
    start hwservicemanager
    mount_all /vendor/etc/fstab.qcom --early
    chown root system /mnt/vendor/persist
    chmod 0771 /mnt/vendor/persist
    restorecon_recursive /mnt/vendor/persist
    mkdir /mnt/vendor/persist/data 0700 system system

on post-fs
    # set RLIMIT_MEMLOCK to 64MB
    setrlimit 8 67108864 67108864

    #for camera cfg dev
    chown system system /sys/bus/platform/drivers/camcfgdev/guard_thermal
    chmod 0664 /sys/bus/platform/drivers/camcfgdev/guard_thermal

on late-fs
    wait_for_prop hwservicemanager.ready true
    #exec_start wait_for_keymaster
    mount_all /vendor/etc/fstab.qcom --late

on post-fs-data
    mkdir /vendor/data/tombstones 0771 system system
    # Enable WLAN cold boot calibration
    write /sys/kernel/cnss/fs_ready 1

on early-boot
    start vendor.sensors
    verity_update_state

on boot
    write /dev/cpuset/audio-app/cpus 1-2
    # Add a cpuset for the camera daemon
    # We want all cores for camera
    mkdir /dev/cpuset/camera-daemon
    write /dev/cpuset/camera-daemon/cpus 0-7
    write /dev/cpuset/camera-daemon/mems 0
    chown cameraserver cameraserver /dev/cpuset/camera-daemon
    chown cameraserver cameraserver /dev/cpuset/camera-daemon/tasks
    chmod 0660 /dev/cpuset/camera-daemon/tasks
    chown system /sys/devices/platform/soc/990000.i2c/i2c-0/0-0038/trusted_touch_enable
    chmod 0660 /sys/devices/platform/soc/990000.i2c/i2c-0/0-0038/trusted_touch_enable
    chown system /sys/devices/platform/soc/990000.spi/spi_master/spi0/spi0.0/trusted_touch_enable
    chmod 0660 /sys/devices/platform/soc/990000.spi/spi_master/spi0/spi0.0/trusted_touch_enable
    chown system /sys/devices/platform/soc/980000.i2c/i2c-0/0-0062/trusted_touch_enable
    chmod 0660 /sys/devices/platform/soc/980000.i2c/i2c-0/0-0062/trusted_touch_enable
    chown system /sys/devices/platform/soc/980000.i2c/i2c-0/0-0062/trusted_touch_event
    chmod 0660 /sys/devices/platform/soc/980000.i2c/i2c-0/0-0062/trusted_touch_event
    chown system /sys/devices/system/cpu/hyp_core_ctl/enable
    chown system /sys/devices/system/cpu/hyp_core_ctl/hcc_min_freq
    #USB controller configuration
    setprop vendor.usb.controller a600000.dwc3

on property:vendor.display.lcd_density=560
   setprop dalvik.vm.heapgrowthlimit 256m

on property:vendor.display.lcd_density=640
   setprop dalvik.vm.heapgrowthlimit 512m

on init && property:ro.boot.mode=charger
    wait_for_prop vendor.all.modules.ready 1
    mount_all /vendor/etc/charger_fw_fstab.qti --early
    wait /sys/kernel/boot_adsp/boot
    write /sys/kernel/boot_adsp/boot 1

on charger
    start vendor.power_off_alarm
    setprop sys.usb.controller a600000.dwc3
    wait /sys/class/udc/${sys.usb.controller}
    setprop sys.usb.configfs 1
    write /sys/kernel/cnss/charger_mode 1

#service vendor.lowi /vendor/bin/sscrpcd
#   class core
#   user system
#   group system wakelock
#   capabilities BLOCK_SUSPEND

#pd-mapper
service vendor.pd_mapper /vendor/bin/pd-mapper
    class core
    user system
    group system

#Peripheral manager
service vendor.per_mgr /vendor/bin/pm-service
    class core
    user system
    group system
    ioprio rt 4

service vendor.per_proxy /vendor/bin/pm-proxy
    class core
    user system
    group system
    disabled

service vendor.mdm_helper /vendor/bin/mdm_helper
    class core
    group system wakelock
    disabled

service vendor.mdm_launcher /vendor/bin/sh /vendor/bin/init.mdm.sh
    class core
    oneshot

on property:init.svc.vendor.per_mgr=running
    start vendor.per_proxy

on property:ro.carrier=wifi-only
    stop vendor.per_mgr
    stop vendor.per_proxy

on property:sys.shutdown.requested=*
    write /sys/kernel/qcom_rproc/shutdown_in_progress 1
    stop vendor.per_proxy

on property:vold.decrypt=trigger_restart_framework
   start vendor.cnss_diag

service vendor.cnss_diag /system/vendor/bin/cnss_diag -q -f -t HELIUM
   class main
   user system
   group system wifi inet sdcard_rw media_rw diag
   oneshot

service unrmd /system/vendor/bin/unrmd
    class late_start
    user root
    group system
    oneshot

on early-boot && property:persist.vendor.pcie.boot_option=*
    write /sys/bus/platform/devices/1c00000.qcom,pcie/debug/boot_option ${persist.vendor.pcie.boot_option}
    write /sys/bus/platform/devices/1c08000.qcom,pcie/debug/boot_option ${persist.vendor.pcie.boot_option}

on property:sys.boot_completed=1
    enable vendor.qvirtmgr
    start  vendor.qvirtmgr
    chmod 0660 /sys/kernel/perfhub/cpuaffinity
    chown system system /sys/kernel/perfhub/cpuaffinity
    chmod 0660 /dev/hw_perf_ctrl
    chown system system /dev/hw_perf_ctrl

# set init privilege for hw_pred_load
    chown system system /sys/kernel/eas/predl_do_predict
    chown system system /sys/kernel/eas/predl_jump_load
    chown system system /sys/kernel/eas/predl_window_size
    chown system system /sys/kernel/eas/predl_enable
    chown system system /sys/kernel/eas/capacity_margin_rt

    chmod 0660 /sys/kernel/eas/predl_do_predict
    chmod 0660 /sys/kernel/eas/predl_jump_load
    chmod 0660 /sys/kernel/eas/predl_window_size
    chmod 0660 /sys/kernel/eas/predl_enable
    chmod 0660 /sys/kernel/eas/capacity_margin_rt
    write /sys/kernel/eas/capacity_margin_rt 1280:1138:1024
# for acm
    chmod 0664 /dev/acm
    chown system system /dev/acm

# for latency_nice
    write /dev/cpuctl/low-background/cpu.latency.nice 10
    write /dev/cpuctl/low-background/cpu.shares 10
    write /dev/cpuctl/high-background/cpu.latency.nice 0
    write /dev/cpuctl/high-background/cpu.shares 1024

# for aisched
on property:sys.boot_completed=1

    chmod 0660 /sys/devices/system/cpu/cpu0/core_ctl/min_cpus
    chown system system /sys/devices/system/cpu/cpu0/core_ctl/min_cpus
    chmod 0660 /sys/devices/system/cpu/cpu0/core_ctl/max_cpus
    chown system system /sys/devices/system/cpu/cpu0/core_ctl/max_cpus
    chmod 0660 /sys/devices/system/cpu/cpu4/core_ctl/min_cpus
    chown system system /sys/devices/system/cpu/cpu4/core_ctl/min_cpus
    chmod 0660 /sys/devices/system/cpu/cpu4/core_ctl/max_cpus
    chown system system /sys/devices/system/cpu/cpu4/core_ctl/max_cpus
    chmod 0660 /sys/devices/system/cpu/cpu7/core_ctl/min_cpus
    chown system system /sys/devices/system/cpu/cpu7/core_ctl/min_cpus
    chmod 0660 /sys/devices/system/cpu/cpu7/core_ctl/max_cpus
    chown system system /sys/devices/system/cpu/cpu7/core_ctl/max_cpus
    chmod 0660 /dev/hw_perf_ctrl
    chown system system /dev/hw_perf_ctrl
    chmod 0660 /sys/kernel/perfhub/cpuaffinity
    chown system system /sys/kernel/perfhub/cpuaffinity

    chmod 0660 /sys/devices/system/cpu/bus_dcvs/DDR/cur_freq
    chown system system /sys/devices/system/cpu/bus_dcvs/DDR/cur_freq

    chown system system /sys/devices/system/cpu/bus_dcvs/LLCC/190b6400.qcom,bwmon-llcc/rw_bytes
    chown system system /sys/devices/system/cpu/bus_dcvs/DDR/19091000.qcom,bwmon-ddr/rw_bytes
    chown system system /sys/devices/system/cpu/bus_dcvs/LLCC/190b6400.qcom,bwmon-llcc/min_freq
    chown system system /sys/devices/system/cpu/bus_dcvs/LLCC/190b6400.qcom,bwmon-llcc/max_freq
    chown system system /sys/devices/system/cpu/bus_dcvs/DDR/19091000.qcom,bwmon-ddr/min_freq
    chown system system /sys/devices/system/cpu/bus_dcvs/DDR/19091000.qcom,bwmon-ddr/max_freq
    chown system system /sys/devices/system/cpu/bus_dcvs/DDR/soc:qcom,memlat:ddr:prime/min_freq
    chown system system /sys/devices/system/cpu/bus_dcvs/DDR/soc:qcom,memlat:ddr:prime/max_freq
    chown system system /sys/devices/system/cpu/bus_dcvs/LLCC/soc:qcom,memlat:llcc:gold/min_freq
    chown system system /sys/devices/system/cpu/bus_dcvs/LLCC/soc:qcom,memlat:llcc:gold/max_freq
    chown system system /sys/devices/system/cpu/bus_dcvs/DDR/soc:qcom,memlat:ddr:gold/min_freq
    chown system system /sys/devices/system/cpu/bus_dcvs/DDR/soc:qcom,memlat:ddr:gold/max_freq

    chmod 0660 /sys/devices/system/cpu/bus_dcvs/LLCC/190b6400.qcom,bwmon-llcc/rw_bytes
    chmod 0660 /sys/devices/system/cpu/bus_dcvs/DDR/19091000.qcom,bwmon-ddr/rw_bytes
    chmod 0660 /sys/devices/system/cpu/bus_dcvs/LLCC/190b6400.qcom,bwmon-llcc/min_freq
    chmod 0660 /sys/devices/system/cpu/bus_dcvs/LLCC/190b6400.qcom,bwmon-llcc/max_freq
    chmod 0660 /sys/devices/system/cpu/bus_dcvs/DDR/19091000.qcom,bwmon-ddr/min_freq
    chmod 0660 /sys/devices/system/cpu/bus_dcvs/DDR/19091000.qcom,bwmon-ddr/max_freq
    chmod 0660 /sys/devices/system/cpu/bus_dcvs/DDR/soc:qcom,memlat:ddr:prime/min_freq
    chmod 0660 /sys/devices/system/cpu/bus_dcvs/DDR/soc:qcom,memlat:ddr:prime/max_freq
    chmod 0660 /sys/devices/system/cpu/bus_dcvs/LLCC/soc:qcom,memlat:llcc:gold/min_freq
    chmod 0660 /sys/devices/system/cpu/bus_dcvs/LLCC/soc:qcom,memlat:llcc:gold/max_freq
    chmod 0660 /sys/devices/system/cpu/bus_dcvs/DDR/soc:qcom,memlat:ddr:gold/min_freq
    chmod 0660 /sys/devices/system/cpu/bus_dcvs/DDR/soc:qcom,memlat:ddr:gold/max_freq

    chown system system /sys/devices/system/cpu/bus_dcvs/DDR/19091000.qcom,bwmon-ddr/ff_frame
    chown system system /sys/devices/system/cpu/bus_dcvs/LLCC/190b6400.qcom,bwmon-llcc/ff_frame
    chown system system /sys/devices/system/cpu/bus_dcvs/memlat_settings/ff_frame

    chmod 0660 /sys/devices/system/cpu/bus_dcvs/DDR/19091000.qcom,bwmon-ddr/ff_frame
    chmod 0660 /sys/devices/system/cpu/bus_dcvs/LLCC/190b6400.qcom,bwmon-llcc/ff_frame
    chmod 0660 /sys/devices/system/cpu/bus_dcvs/memlat_settings/ff_frame

# set huawei schedutil init privilege
    write /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor schedutil
    write /sys/devices/system/cpu/cpu4/cpufreq/scaling_governor schedutil
    write /sys/devices/system/cpu/cpu7/cpufreq/scaling_governor schedutil

    chown system system /sys/devices/system/cpu/cpufreq/policy0/schedutil/above_hispeed_delay
    chown system system /sys/devices/system/cpu/cpufreq/policy0/schedutil/boostpulse_duration
    chown system system /sys/devices/system/cpu/cpufreq/policy0/schedutil/freq_reporting_policy
    chown system system /sys/devices/system/cpu/cpufreq/policy0/schedutil/go_hispeed_load
    chown system system /sys/devices/system/cpu/cpufreq/policy0/schedutil/hispeed_freq
    chown system system /sys/devices/system/cpu/cpufreq/policy0/schedutil/iowait_boost_step
    chown system system /sys/devices/system/cpu/cpufreq/policy0/schedutil/min_sample_time
    chown system system /sys/devices/system/cpu/cpufreq/policy0/schedutil/target_loads
    chown system system /sys/devices/system/cpu/cpufreq/policy0/schedutil/timer_slack
    chown system system /sys/devices/system/cpu/cpufreq/policy0/schedutil/fast_ramp_down
    chown system system /sys/devices/system/cpu/cpufreq/policy0/schedutil/fast_ramp_up
    chown system system /sys/devices/system/cpu/cpufreq/policy0/schedutil/top_task_stats_policy
    chown system system /sys/devices/system/cpu/cpufreq/policy0/schedutil/top_task_stats_empty_window
    chown system system /sys/devices/system/cpu/cpufreq/policy0/schedutil/top_task_hist_size
    chown system system /sys/devices/system/cpu/cpufreq/policy0/schedutil/governor_door

    chown system system /sys/devices/system/cpu/cpufreq/policy4/schedutil/above_hispeed_delay
    chown system system /sys/devices/system/cpu/cpufreq/policy4/schedutil/boostpulse_duration
    chown system system /sys/devices/system/cpu/cpufreq/policy4/schedutil/freq_reporting_policy
    chown system system /sys/devices/system/cpu/cpufreq/policy4/schedutil/go_hispeed_load
    chown system system /sys/devices/system/cpu/cpufreq/policy4/schedutil/hispeed_freq
    chown system system /sys/devices/system/cpu/cpufreq/policy4/schedutil/iowait_boost_step
    chown system system /sys/devices/system/cpu/cpufreq/policy4/schedutil/min_sample_time
    chown system system /sys/devices/system/cpu/cpufreq/policy4/schedutil/target_loads
    chown system system /sys/devices/system/cpu/cpufreq/policy4/schedutil/timer_slack
    chown system system /sys/devices/system/cpu/cpufreq/policy4/schedutil/fast_ramp_down
    chown system system /sys/devices/system/cpu/cpufreq/policy4/schedutil/fast_ramp_up
    chown system system /sys/devices/system/cpu/cpufreq/policy4/schedutil/top_task_stats_policy
    chown system system /sys/devices/system/cpu/cpufreq/policy4/schedutil/top_task_stats_empty_window
    chown system system /sys/devices/system/cpu/cpufreq/policy4/schedutil/top_task_hist_size
    chown system system /sys/devices/system/cpu/cpufreq/policy4/schedutil/governor_door

    chown system system /sys/devices/system/cpu/cpufreq/policy7/schedutil/above_hispeed_delay
    chown system system /sys/devices/system/cpu/cpufreq/policy7/schedutil/boostpulse_duration
    chown system system /sys/devices/system/cpu/cpufreq/policy7/schedutil/freq_reporting_policy
    chown system system /sys/devices/system/cpu/cpufreq/policy7/schedutil/go_hispeed_load
    chown system system /sys/devices/system/cpu/cpufreq/policy7/schedutil/hispeed_freq
    chown system system /sys/devices/system/cpu/cpufreq/policy7/schedutil/iowait_boost_step
    chown system system /sys/devices/system/cpu/cpufreq/policy7/schedutil/min_sample_time
    chown system system /sys/devices/system/cpu/cpufreq/policy7/schedutil/target_loads
    chown system system /sys/devices/system/cpu/cpufreq/policy7/schedutil/timer_slack
    chown system system /sys/devices/system/cpu/cpufreq/policy7/schedutil/fast_ramp_down
    chown system system /sys/devices/system/cpu/cpufreq/policy7/schedutil/fast_ramp_up
    chown system system /sys/devices/system/cpu/cpufreq/policy7/schedutil/top_task_stats_policy
    chown system system /sys/devices/system/cpu/cpufreq/policy7/schedutil/top_task_stats_empty_window
    chown system system /sys/devices/system/cpu/cpufreq/policy7/schedutil/top_task_hist_size
    chown system system /sys/devices/system/cpu/cpufreq/policy7/schedutil/governor_door

    chmod 660 /sys/devices/system/cpu/cpufreq/policy0/schedutil/above_hispeed_delay
    chmod 660 /sys/devices/system/cpu/cpufreq/policy0/schedutil/boostpulse_duration
    chmod 660 /sys/devices/system/cpu/cpufreq/policy0/schedutil/freq_reporting_policy
    chmod 660 /sys/devices/system/cpu/cpufreq/policy0/schedutil/go_hispeed_load
    chmod 660 /sys/devices/system/cpu/cpufreq/policy0/schedutil/hispeed_freq
    chmod 660 /sys/devices/system/cpu/cpufreq/policy0/schedutil/iowait_boost_step
    chmod 660 /sys/devices/system/cpu/cpufreq/policy0/schedutil/min_sample_time
    chmod 660 /sys/devices/system/cpu/cpufreq/policy0/schedutil/target_loads
    chmod 660 /sys/devices/system/cpu/cpufreq/policy0/schedutil/timer_slack
    chmod 660 /sys/devices/system/cpu/cpufreq/policy0/schedutil/fast_ramp_down
    chmod 660 /sys/devices/system/cpu/cpufreq/policy0/schedutil/fast_ramp_up
    chmod 660 /sys/devices/system/cpu/cpufreq/policy0/schedutil/top_task_stats_policy
    chmod 660 /sys/devices/system/cpu/cpufreq/policy0/schedutil/top_task_stats_empty_window
    chmod 660 /sys/devices/system/cpu/cpufreq/policy0/schedutil/top_task_hist_size
    chmod 660 /sys/devices/system/cpu/cpufreq/policy0/schedutil/governor_door

    chmod 660 /sys/devices/system/cpu/cpufreq/policy4/schedutil/above_hispeed_delay
    chmod 660 /sys/devices/system/cpu/cpufreq/policy4/schedutil/boostpulse_duration
    chmod 660 /sys/devices/system/cpu/cpufreq/policy4/schedutil/freq_reporting_policy
    chmod 660 /sys/devices/system/cpu/cpufreq/policy4/schedutil/go_hispeed_load
    chmod 660 /sys/devices/system/cpu/cpufreq/policy4/schedutil/hispeed_freq
    chmod 660 /sys/devices/system/cpu/cpufreq/policy4/schedutil/iowait_boost_step
    chmod 660 /sys/devices/system/cpu/cpufreq/policy4/schedutil/min_sample_time
    chmod 660 /sys/devices/system/cpu/cpufreq/policy4/schedutil/target_loads
    chmod 660 /sys/devices/system/cpu/cpufreq/policy4/schedutil/timer_slack
    chmod 660 /sys/devices/system/cpu/cpufreq/policy4/schedutil/fast_ramp_down
    chmod 660 /sys/devices/system/cpu/cpufreq/policy4/schedutil/fast_ramp_up
    chmod 660 /sys/devices/system/cpu/cpufreq/policy4/schedutil/top_task_stats_policy
    chmod 660 /sys/devices/system/cpu/cpufreq/policy4/schedutil/top_task_stats_empty_window
    chmod 660 /sys/devices/system/cpu/cpufreq/policy4/schedutil/top_task_hist_size
    chmod 660 /sys/devices/system/cpu/cpufreq/policy4/schedutil/governor_door

    chmod 660 /sys/devices/system/cpu/cpufreq/policy7/schedutil/above_hispeed_delay
    chmod 660 /sys/devices/system/cpu/cpufreq/policy7/schedutil/boostpulse_duration
    chmod 660 /sys/devices/system/cpu/cpufreq/policy7/schedutil/freq_reporting_policy
    chmod 660 /sys/devices/system/cpu/cpufreq/policy7/schedutil/go_hispeed_load
    chmod 660 /sys/devices/system/cpu/cpufreq/policy7/schedutil/hispeed_freq
    chmod 660 /sys/devices/system/cpu/cpufreq/policy7/schedutil/iowait_boost_step
    chmod 660 /sys/devices/system/cpu/cpufreq/policy7/schedutil/min_sample_time
    chmod 660 /sys/devices/system/cpu/cpufreq/policy7/schedutil/target_loads
    chmod 660 /sys/devices/system/cpu/cpufreq/policy7/schedutil/timer_slack
    chmod 660 /sys/devices/system/cpu/cpufreq/policy7/schedutil/fast_ramp_down
    chmod 660 /sys/devices/system/cpu/cpufreq/policy7/schedutil/fast_ramp_up
    chmod 660 /sys/devices/system/cpu/cpufreq/policy7/schedutil/top_task_stats_policy
    chmod 660 /sys/devices/system/cpu/cpufreq/policy7/schedutil/top_task_stats_empty_window
    chmod 660 /sys/devices/system/cpu/cpufreq/policy7/schedutil/top_task_hist_size
    chmod 660 /sys/devices/system/cpu/cpufreq/policy7/schedutil/governor_door

    chown system system /sys/devices/system/cpu/cpufreq/policy0/scaling_governor
    chown system system /sys/devices/system/cpu/cpufreq/policy0/scaling_min_freq
    chown system system /sys/devices/system/cpu/cpufreq/policy0/scaling_max_freq

    chown system system /sys/devices/system/cpu/cpufreq/policy4/scaling_governor
    chown system system /sys/devices/system/cpu/cpufreq/policy4/scaling_min_freq
    chown system system /sys/devices/system/cpu/cpufreq/policy4/scaling_max_freq

    chown system system /sys/devices/system/cpu/cpufreq/policy7/scaling_governor
    chown system system /sys/devices/system/cpu/cpufreq/policy7/scaling_min_freq
    chown system system /sys/devices/system/cpu/cpufreq/policy7/scaling_max_freq

    chmod 0660 /sys/devices/system/cpu/cpufreq/policy0/scaling_governor
    chmod 0660 /sys/devices/system/cpu/cpufreq/policy0/scaling_min_freq
    chmod 0660 /sys/devices/system/cpu/cpufreq/policy0/scaling_max_freq
    chmod 0660 /sys/devices/system/cpu/cpufreq/policy4/scaling_governor
    chmod 0660 /sys/devices/system/cpu/cpufreq/policy4/scaling_min_freq
    chmod 0660 /sys/devices/system/cpu/cpufreq/policy4/scaling_max_freq
    chmod 0660 /sys/devices/system/cpu/cpufreq/policy7/scaling_governor
    chmod 0660 /sys/devices/system/cpu/cpufreq/policy7/scaling_min_freq
    chmod 0660 /sys/devices/system/cpu/cpufreq/policy7/scaling_max_freq

# for aisched
    chown system system /sys/devices/platform/soc/soc:qcom,cpu-cpu-llcc-bw/devfreq/soc:qcom,cpu-cpu-llcc-bw/min_freq
    chown system system /sys/devices/platform/soc/soc:qcom,cpu-cpu-llcc-bw/devfreq/soc:qcom,cpu-cpu-llcc-bw/max_freq
    chown system system /sys/devices/platform/soc/soc:qcom,cpu-llcc-ddr-bw/devfreq/soc:qcom,cpu-llcc-ddr-bw/min_freq
    chown system system /sys/devices/platform/soc/soc:qcom,cpu-llcc-ddr-bw/devfreq/soc:qcom,cpu-llcc-ddr-bw/max_freq
    chown system system /sys/devices/platform/soc/soc:qcom,cpu4-cpu-llcc-lat/devfreq/soc:qcom,cpu4-cpu-llcc-lat/min_freq
    chown system system /sys/devices/platform/soc/soc:qcom,cpu4-cpu-llcc-lat/devfreq/soc:qcom,cpu4-cpu-llcc-lat/max_freq
    chown system system /sys/devices/platform/soc/soc:qcom,cpu4-llcc-ddr-lat/devfreq/soc:qcom,cpu4-llcc-ddr-lat/min_freq
    chown system system /sys/devices/platform/soc/soc:qcom,cpu4-llcc-ddr-lat/devfreq/soc:qcom,cpu4-llcc-ddr-lat/max_freq

    chmod 0660 /sys/devices/platform/soc/soc:qcom,cpu-cpu-llcc-bw/devfreq/soc:qcom,cpu-cpu-llcc-bw/min_freq
    chmod 0660 /sys/devices/platform/soc/soc:qcom,cpu-cpu-llcc-bw/devfreq/soc:qcom,cpu-cpu-llcc-bw/max_freq
    chmod 0660 /sys/devices/platform/soc/soc:qcom,cpu-llcc-ddr-bw/devfreq/soc:qcom,cpu-llcc-ddr-bw/min_freq
    chmod 0660 /sys/devices/platform/soc/soc:qcom,cpu-llcc-ddr-bw/devfreq/soc:qcom,cpu-llcc-ddr-bw/max_freq
    chmod 0660 /sys/devices/platform/soc/soc:qcom,cpu4-cpu-llcc-lat/devfreq/soc:qcom,cpu4-cpu-llcc-lat/min_freq
    chmod 0660 /sys/devices/platform/soc/soc:qcom,cpu4-cpu-llcc-lat/devfreq/soc:qcom,cpu4-cpu-llcc-lat/max_freq
    chmod 0660 /sys/devices/platform/soc/soc:qcom,cpu4-llcc-ddr-lat/devfreq/soc:qcom,cpu4-llcc-ddr-lat/min_freq
    chmod 0660 /sys/devices/platform/soc/soc:qcom,cpu4-llcc-ddr-lat/devfreq/soc:qcom,cpu4-llcc-ddr-lat/max_freq
